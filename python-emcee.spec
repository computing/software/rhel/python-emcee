%global srcname emcee

Name:		python-%{srcname}
Version:	2.2.1
Release:	3%{?dist}
Summary:	Monte Carlo sampler

License:	BSD
URL:		https://emcee.readthedocs.io/
Source0:	https://github.com/dfm/emcee/archive/v2.2.1.tar.gz

BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-setuptools
BuildRequires:	python%{python3_pkgversion}-numpy

%description
pure-Python implementation of Goodman & Weare’s Affine Invariant Markov chain Monte Carlo (MCMC) Ensemble sampler

%package -n python%{python3_pkgversion}-%{srcname}
Summary:	%{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname}
pure-Python implementation of Goodman & Weare’s Affine Invariant Markov chain Monte Carlo (MCMC) Ensemble sampler


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build


%install
%py3_install

%check
%{__python3} setup.py test


%files -n python%{python3_pkgversion}-%{srcname}
%doc
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}*egg-info/


%changelog
* Fri Oct 4 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 2.2.1-3
- Drop python3.4 support

* Wed Nov 14 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 2.2.1-2
- Add python3.6 support

* Fri Nov 2 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 2.2.1-1
- Initial package
